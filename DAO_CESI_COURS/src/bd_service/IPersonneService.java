package bd_service;

import java.util.ArrayList;

import bd_entity.Personne;

public interface IPersonneService {

	public ArrayList<Personne> getAll();
	
	public void createNew(Personne p);
}
