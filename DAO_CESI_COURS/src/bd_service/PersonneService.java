package bd_service;

import java.sql.SQLException;
import java.util.ArrayList;

import bd_dao.IPersonneDAO;
import bd_dao.PersonneDAO;
import bd_entity.Personne;

public class PersonneService implements IPersonneService {

	private IPersonneDAO personneDAO;
	
	public PersonneService() throws SQLException {
		this.personneDAO = new PersonneDAO();
	}
	
	@Override
	public ArrayList<Personne> getAll() {
		return this.personneDAO.selectAll();
	}

	@Override
	public void createNew(Personne p) {
		this.personneDAO.insert(p);
	}

}
