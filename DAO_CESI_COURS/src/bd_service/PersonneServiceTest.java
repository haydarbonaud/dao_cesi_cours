package bd_service;

import java.util.ArrayList;

import bd_entity.Personne;

public class PersonneServiceTest implements IPersonneService {

	ArrayList<Personne> tmp ;
	
	public PersonneServiceTest() {
		tmp = new ArrayList<>();
	}
	
	@Override
	public ArrayList<Personne> getAll() {
		tmp.add(new Personne(1, "James", "Bond"));
		tmp.add(new Personne(2, "Mary", "Poppins"));
		tmp.add(new Personne(3, "Lino", "Ventura"));
		return tmp;
	}

	@Override
	public void createNew(Personne p) {
		tmp.add(p);
	}

}
