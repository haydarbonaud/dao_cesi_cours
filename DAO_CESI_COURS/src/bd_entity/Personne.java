package bd_entity;

public class Personne {

	private long id;
	private String name;
	private String fName;
	
	public Personne() { }
	
	public Personne(int id, String name, String fName) {
		this.id = id;
		this.name = name;
		this.fName = fName;
	}

	public long getId() {
		return id;
	}
	public void setId(long l) {
		this.id = l;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	
	@Override
	public String toString() {
		return this.getId() + " - " + this.getName() + " - " + this.getfName();
	}
}
