package bd_dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bd_entity.Personne;

public class PersonneDAO implements IPersonneDAO {

	private DBAccess db;
	private ResultSet rs;
	private ArrayList<Personne> personnes;

	public PersonneDAO() throws SQLException {
		db = DBAccess.getInstance();
	}

	private void createPersonnesFromRS() {
		
		try {
			while (rs.next()) {
				Personne p = new Personne();
				p.setId(rs.getLong("id"));
				p.setName(rs.getString("name"));
				p.setfName(rs.getString("firstname"));
				this.getPersonnes().add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public ArrayList<Personne> selectAll() {
		try {
			rs = db.m_getRowsRows("call selectAllPersonnes()");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		this.createPersonnesFromRS();
		return this.personnes;
	}

	@Override
	public void insert(Personne p) {
		// TODO Auto-generated method stub

	}
	
	private ArrayList<Personne> getPersonnes(){
		if (this.personnes == null) {
			this.personnes = new ArrayList<Personne>();
		}
		return this.personnes;
	}

}
