package bd_dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBAccess {
	private String connectionUrl;
	private String login;
	private String psw;
	private Connection con;
	private Statement stmt;
	private ResultSet rs;
	
	public static DBAccess Instance;

	private DBAccess() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		this.connectionUrl = "jdbc:mysql://localhost/bdd_09_2019?serverTimezone=UTC";
		this.login = "root";
		this.psw = "root";
		this.con = DriverManager.getConnection(connectionUrl, login, psw);
	}

	public ResultSet m_getRowsRows(String rq_sql) throws SQLException {
		this.stmt = con.createStatement();
		this.rs = stmt.executeQuery(rq_sql);

		return this.rs;
	}

	public void m_ActionsRows(String rq_sql) throws SQLException {
		this.stmt = con.createStatement();
		this.stmt.executeUpdate(rq_sql);
	}
	
	public static DBAccess getInstance() {
		if (Instance == null) {
			try {
				Instance = new DBAccess();
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return Instance;
	}
}
