package bd_dao;

import java.util.ArrayList;

import bd_entity.Personne;

public interface IPersonneDAO {

	public ArrayList<Personne> selectAll();
	
	public void insert(Personne p);
}
