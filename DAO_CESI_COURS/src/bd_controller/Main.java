package bd_controller;

import java.sql.SQLException;

import bd_entity.Personne;
import bd_service.IPersonneService;
import bd_service.PersonneService;
import bd_service.PersonneServiceTest;

public class Main {

	private static IPersonneService personneService;
	
	public static void main(String[] args) throws SQLException {

		// personneService = new PersonneServiceTest();
		personneService = new PersonneService();
		
		for (Personne p : personneService.getAll()) {
			System.out.println(p);
		}

	}

}
